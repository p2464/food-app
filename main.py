#import pysimplegui as sg
#declares variables of the program
import _tkinter as tk
import threading
from test import window

#takes input_groceries and updates list
def update_groceries(new_grocery, grocery_list):
    grocery_list.append(new_grocery)

#takes input of new groceries then calls update_groceries to update the list
def input_groceries(grocery_list):

    grocery = input("What is the name of your grocery? \n")

    b = int(input("What is the weight (in lb) of your grocery? \n"))
    if b >= 0:
        lb = b
    else:
        print("You did not input a positive value for weight")

    c = int(input("What is the volume (in oz) of your grocery?\n"))
    if c >= 0:
        oz = c
    else:
        print("You input a negative number for the volume")

    d = int(input("What is the quantity of your grocery?"))
    if d >= 0:
        qty = d
    else:
        print("You entered a negative quantity")

    new_grocery = [grocery, lb, oz, qty]
    update_groceries(new_grocery, grocery_list)

#Takes in grocery and quantities and if it is found in grocery_list updates those quantities
def update_quantities(grocery, grocery_list, lb, oz ,qty):
    for x in grocery_list:
        if grocery_list[x][0] == grocery:
            y = 1
            while y < 4:
                if y == 1:
                    if grocery_list[x][y] > 0:
                        if grocery_list[x][y] - lb > 0:
                            grocery_list[x][y] = grocery_list[x][y] - lb
                        else:
                            quantity = grocery_list[x][y] - lb
                            more_ingredient(grocery, y, quantity)
                if y == 2:
                    if grocery_list[x][y] > 0:
                        if grocery_list[x][y] - oz > 0:
                            grocery_list[x][y] = grocery_list[x][y] - oz
                        else:
                            quantity = grocery_list[x][y] - oz
                            more_ingredient(grocery, y, quantity)
                if y == 3:
                    if grocery_list[x][y] > 0:
                        if grocery_list[x][y] - qty > 0:
                            grocery_list[x][y] = grocery_list[x][y] - qty
                        else:
                            quantity = grocery_list[x][y] - qty
                            more_ingredient(grocery, y, quantity)
                y = y + 1

#Checks if list is empty and if it is, removes grocery from list of materials
def is_empty(grocery, x, grocery_list):
    for x in grocery_list:
        if grocery_list[x][0] == grocery:
            if grocery_list[x][1] == 0 and grocery_list[x][2] == 0 and grocery_list[x][3] == 0:
                grocery_list.pop(x)

#lets user know you need more of an ingredient
def more_ingredient(grocery, y, quantity):
    if y == 1:
        return("You need " + quantity + " more lbs of " + grocery)
    if y == 2:
        return("You need " + quantity + " more oz of " + grocery)
    if y == 3:
        return("You need " + quantity + " more of " + grocery)

#updates recipe list with new recipe
def update_recipes(recipe, recipe_list):
    recipe_list.append(recipe)

#takes a recipe in then calls update recipes 
def input_recipes():
    new_recipe = []
    new_recipe.append(input("Name of recipe?\n"))
    ingredient = True
    #issue is if someone types in a string instead of int or vice versa. Might need to make a new function just to check for this
    while ingredient == True:
        new_recipe.append(input("What is the name of your ingredient?\n"))
        new_recipe.append(int(input("What is the weight (in lb) of your ingredient?\n")))
        new_recipe.append(int(input("What is the volume (in oz) of your ingredient?\n")))
        new_recipe.append(int(input("What is the quantity of your ingredient?\n")))
        ingredient = input("Do you have more ingredients? Y/N \n")
    #new_recipe is filled with data and can then be sent to update_recipes
    update_recipes(new_recipe)

#Takes inputs to figure out which functions to move forward with
def main():

    window_thread = threading.Thread(target = window)
    window_thread.start()
    #hello justin
    # grocery_list = []
    # recipe_list = []
    # grocery = ""
    # lb, oz, qty = 0, 0, 0
    # new_grocery = True
    # while new_grocery == True:
    #     input_groceries(grocery_list)
    #     if input("Do you have new groceries Y/N \n") == "Y":
    #         new_grocery = True
    #     else:
    #         new_grocery = False

if __name__ == "__main__":
    main()